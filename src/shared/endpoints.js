const BASE_ENDPOINT = 'https://api.github.com/';
const REPOS_ENDPOINT = 'https://api.github.com/repos/';
const USER_ENDPOINT = 'https://api.github.com/users/';

export { BASE_ENDPOINT, REPOS_ENDPOINT, USER_ENDPOINT };
