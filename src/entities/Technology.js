class Technology {
  constructor(techName, size) {
    this.techName = techName;
    this.size = size;
  }
}

export { Technology };
