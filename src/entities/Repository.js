import { User } from './User';

class Repository {
  constructor(repoId, repoName, description, userId, userName, avatarUrl) {
    this.repoId = repoId;
    this.repoName = repoName;
    this.description = description;
    this.owner = new User(userId, userName, avatarUrl);
  }
}

export { Repository };
