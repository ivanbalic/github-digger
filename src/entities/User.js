class User {
  constructor(
    userId,
    userName,
    avatarUrl,
    fullName,
    email,
    bio,
    followers,
    following,
    location,
    blog
  ) {
    this.userId = userId;
    this.userName = userName;
    this.avatarUrl = avatarUrl;
    this.fullName = fullName;
    this.email = email;
    this.bio = bio;
    this.followers = followers;
    this.following = following;
    this.location = location;
    this.blog = blog;
  }
}

export { User };
