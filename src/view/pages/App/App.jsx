import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import UserPage from '../User/UserPageContainer';
import { HomePage } from '../Home/HomePage';
import SearchPage from '../Search/SearchPageContainer';
import { Header } from '../../components/Header/Header';
import RepositoryPage from '../Repository/RepositoryPageContainer';
import ErrorBoundary from '../../components/ErrorBoundary/ErrorBoundaryContainer';

class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <main className="container">
          <ErrorBoundary>
            <Switch>
              <Route path="/user/:userName" component={UserPage} />
              <Route path="/repository" component={RepositoryPage} />
              <Route path="/search" component={SearchPage} />
              <Route path="/" component={HomePage} />
            </Switch>
          </ErrorBoundary>
        </main>
      </Fragment>
    );
  }
}

export { App };
