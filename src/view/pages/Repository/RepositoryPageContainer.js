import { connect } from 'react-redux';

import { RepositoryPage } from './RepositoryPage';
import { startLoading } from '../../../state/actions/loaderActions';
import { searchSectionData } from '../../../state/actions/repositoryActions';

const mapStateToProps = state => {
  return {
    loaderState: state.loaderReducer,
    repoState: state.repositoryReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadData: (userName, repositoryName) =>
      dispatch(searchSectionData(userName, repositoryName)),
    startLoader: () => dispatch(startLoading())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RepositoryPage);
