import React from 'react';
import PropTypes from 'prop-types';

import { Message } from '../../../components/Message/Message';

import classes from './TechnologiesSection.module.scss';

const TechnologiesSection = ({ technologies }) => {
  const { tech } = classes;
  return (
    <div className="col-6 border-right p-4">
      <h3 className="mb-4 border-bottom pb-3">Technologies</h3>
      {technologies.length === 0 ? (
        <Message className="text-secondary" text="No data about technologies" />
      ) : (
        <ul className="list-group list-group-flush">
          {technologies.map((singleTech, index) => {
            const { techName, size } = singleTech;
            return (
              <li className={`list-group-item ${tech}`} key={index}>
                {techName}
                <span className="float-right">{size} bytes</span>
              </li>
            );
          })}
        </ul>
      )}
    </div>
  );
};

TechnologiesSection.propsTypes = {
  technologies: PropTypes.array
};

export { TechnologiesSection };
