import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import { UserInfo } from './UserInfo/UserInfo';
import { Loader } from '../../components/Loader/Loader';
import { ContributorsSection } from './ContributorsSection/ContributorsSection';
import { TechnologiesSection } from './TechnologiesSection/TechnologiesSection';

class RepositoryPage extends Component {
  static propTypes = {
    repoState: PropTypes.object,
    loaderState: PropTypes.object,
    loadData: PropTypes.func,
    startLoader: PropTypes.func
  };

  componentDidMount() {
    const { location, loadData } = this.props;
    const { userName, repositoryName } = location.state;
    loadData(userName, repositoryName);
  }

  render() {
    const { location, loaderState, repoState, startLoader } = this.props;
    const { technologies, contributors } = repoState;

    return (
      <Fragment>
        <UserInfo
          data={location.state}
          handleLinkClick={this.props.startLoader}
        />
        {loaderState.isLoading ? (
          <div className="row">
            <Loader />
          </div>
        ) : (
          <div className="row">
            <TechnologiesSection technologies={technologies} />
            <ContributorsSection
              contributors={contributors}
              handleLinkClick={startLoader}
            />
          </div>
        )}
      </Fragment>
    );
  }
}

export { RepositoryPage };
