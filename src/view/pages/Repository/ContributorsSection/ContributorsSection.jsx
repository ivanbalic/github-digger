import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../../components/Link/Link';

import { Message } from '../../../components/Message/Message';

import classes from './ContributorsSection.module.scss';

const ContributorsSection = ({ contributors, handleLinkClick }) => {
  return (
    <div className="col-6 p-4">
      <h3 className="mb-4 border-bottom pb-3">Contributors</h3>
      {contributors.length === 0 ? (
        <Message className="text-secondary" text="No data about contributors" />
      ) : (
        contributors.map(({ userName, avatarUrl }, index) => {
          return (
            <div
              key={index}
              className="my-4 d-flex flex-row align-items-center"
            >
              <img
                src={avatarUrl}
                className={`${classes.width_10} mr-3 rounded-circle`}
                alt="user"
              />
              <Link path={`user/${userName}`}>
                <h5 className="align-middle ">{userName}</h5>
              </Link>
            </div>
          );
        })
      )}
    </div>
  );
};

ContributorsSection.propTypes = {
  contributors: PropTypes.array,
  handleDetailsClick: PropTypes.func
};

export { ContributorsSection };
