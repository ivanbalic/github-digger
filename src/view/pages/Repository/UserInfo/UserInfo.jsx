import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../../components/Link/Link';

import classes from './UserInfo.module.scss';

const UserInfo = ({ data, handleLinkClick }) => {
  const { clear, width_20 } = classes;
  const { repositoryName, userName, avatarUrl } = data;
  return (
    <div className="row">
      <div className="col-12 mt-5 mb-3">
        <h1 className="mt-0 mb-1">{repositoryName}</h1>
        <img
          className={`rounded-circle float-right ${width_20}`}
          src={avatarUrl}
          alt="profile"
        />
        <Link path={`user/${userName}`} onClick={handleLinkClick}>
          {' '}
          <h3 className={`float-right ${clear} ${width_20} mt-2 text-center`}>
            {userName}
          </h3>
        </Link>
      </div>
    </div>
  );
};

UserInfo.propTypes = {
  data: PropTypes.object,
  handleLinkClick: PropTypes.func
};

export { UserInfo };
