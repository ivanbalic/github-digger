import React, { Component, Fragment } from 'react';

import { PageCover } from './PageCover/PageCover';
import SearchBar from '../../components/SearchBar/SearchBarContainer';

class HomePage extends Component {
  handleSearch = () => {
    this.props.history.push('/search');
  };

  render() {
    return (
      <Fragment>
        <SearchBar handleSearch={this.handleSearch} />
        <PageCover />
      </Fragment>
    );
  }
}

export { HomePage };
