import React from 'react';

const PageCover = () => {
  return (
    <div className="jumbotron jumbotron-fluid">
      <div className="container">
        <h1 className="display-4 border-bottom border-secondary pb-4">
          <i className="fab fa-github" /> GitHub Digger
        </h1>
        <p className="lead">
          <strong>
            GitHub Digger is simple tool for searching through the public
            repositories.
          </strong>
          <br /> Search will give you 100 best matches to your search term and
          display them as list of cards. Each card will have 'More Details'
          option. By clicking 'More Details' you will be redirected to a new
          page where details, such as technologies and contributors, will be
          shown. From that page you will be able to click on each contributor
          and view details about that user.
        </p>
      </div>
    </div>
  );
};

export { PageCover };
