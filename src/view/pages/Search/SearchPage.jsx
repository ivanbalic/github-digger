import React, { Component, Fragment } from 'react';
import { PropTypes } from 'prop-types';

import { Message } from '../../components/Message/Message';
import SearchBar from '../../components/SearchBar/SearchBarContainer';
import { RepositoryCard } from '../../components/RepositoryCard/RepositoryCard';

class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleDetailsClick = this.handleDetailsClick.bind(this);
  }

  static propTypes = {
    searchState: PropTypes.object,
    loaderState: PropTypes.object,
    repoState: PropTypes.object,
    errorState: PropTypes.object,
    loadData: PropTypes.func
  };

  handleDetailsClick(repositoryName, userName, avatarUrl) {
    this.props.history.push({
      pathname: '/repository',
      state: {
        repositoryName,
        userName,
        avatarUrl
      }
    });
  }

  handleSearch() {
    const { searchState, loadData } = this.props;
    loadData(searchState.inputValue);
  }

  componentDidMount() {
    this.handleSearch();
  }

  render() {
    const { repoState, loaderState, errorState } = this.props;
    const { handleSearch, handleDetailsClick } = this;
    const { repositories } = repoState;

    if (errorState) {
      throw new Error(errorState.message);
    }

    return (
      <Fragment>
        <SearchBar
          handleSearch={handleSearch}
          isLoading={loaderState.isLoading}
        />

        {repositories && (
          <div className="col-12">
            {repositories.map(repo => {
              return (
                <RepositoryCard
                  key={repo.repoId}
                  repo={repo}
                  className="border bg-light my-1 rounded"
                  handleDetailsClick={handleDetailsClick}
                />
              );
            })}
          </div>
        )}
        {repositories && repositories.length === 0 ? (
          <Message
            className="text-secondary display-4 text-center"
            text="Sorry, we couldn't find any match"
          />
        ) : null}
      </Fragment>
    );
  }
}

export { SearchPage };
