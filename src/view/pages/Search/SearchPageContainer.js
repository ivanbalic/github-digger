import { connect } from 'react-redux';
import { SearchPage } from './SearchPage';
import { searchRepositories } from '../../../state/actions/repositoryActions';

const mapStateToProps = state => {
  return {
    searchState: state.searchReducer,
    loaderState: state.loaderReducer,
    repoState: state.repositoryReducer,
    errorState: state.errorReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadData: searchTerm => dispatch(searchRepositories(searchTerm))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchPage);
