import React from 'react';
import { PropTypes } from 'prop-types';

const ContactInfo = ({ text, iconClass, className }) => {
  return (
    <div className={className}>
      <i className={iconClass} />
      {` ${text}`}
    </div>
  );
};

ContactInfo.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  iconClass: PropTypes.string
};

export { ContactInfo };
