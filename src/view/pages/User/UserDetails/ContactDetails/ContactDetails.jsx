import React from 'react';
import { PropTypes } from 'prop-types';

import { ContactInfo } from './ContactInfo/ContactInfo';

const ContactDetails = ({ email, blog, location, className }) => {
  return (
    <div className={className}>
      {email ? <ContactInfo text={email} iconClass="fas fa-envelope" /> : null}
      {blog ? <ContactInfo text={blog} iconClass="fas fa-globe" /> : null}
      {location ? (
        <ContactInfo text={location} iconClass="fas fa-map-marker-alt" />
      ) : null}
    </div>
  );
};

ContactDetails.propTypes = {
  email: PropTypes.string,
  blog: PropTypes.string,
  location: PropTypes.string,
  className: PropTypes.string
};

export { ContactDetails };
