import React from 'react';
import { PropTypes } from 'prop-types';

import { Bio } from './Bio/Bio';
import { Button } from '../../../components/Button/Button';
import { ContactDetails } from './ContactDetails/ContactDetails';

import classes from './UserDetails.module.scss';

const UserDetails = ({ user, className }) => {
  const {
    userName,
    fullName,
    avatarUrl,
    email,
    followers,
    following,
    bio,
    location,
    blog
  } = user;
  return (
    <div className={`jumbotron ${className}`}>
      <div className="d-flex flex-row">
        <div className={`d-flex flex-column ${classes.width_30}`}>
          <img src={avatarUrl} className="rounded-circle w-100" alt="user" />
          <Button
            text={`Followers: ${followers}`}
            className="btn btn-secondary my-2 mx-1"
          />
          <Button
            text={`Following: ${following}`}
            className="btn btn-secondary my-2"
          />
        </div>
        <div className="ml-4 align-self-start">
          <h1 className="display-4">{fullName}</h1>
          <h5>{userName}</h5>
        </div>
        <ContactDetails
          className="align-self-end ml-auto"
          email={email}
          location={location}
          blog={blog}
        />
      </div>
      {bio ? <Bio text={bio} /> : null}
    </div>
  );
};

UserDetails.propTypes = {
  user: PropTypes.object,
  className: PropTypes.string
};

export { UserDetails };
