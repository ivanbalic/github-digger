import React from 'react';
import { PropTypes } from 'prop-types';

const Bio = ({ text }) => {
  return (
    <div>
      <hr className="my-4" />
      <h5 className="my2">Bio:</h5>
      <p>{text}</p>
    </div>
  );
};

Bio.propTypes = {
  text: PropTypes.string
};

export { Bio };
