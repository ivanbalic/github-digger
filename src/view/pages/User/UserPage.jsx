import React, { Component, Fragment } from 'react';
import { PropTypes } from 'prop-types';

import { UserDetails } from './UserDetails/UserDetails';
import { Loader } from '../../components/Loader/Loader';
import { RepositoriesSection } from './RepositoriesSection/RepositoriesSection';

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.handleDetailsClick = this.handleDetailsClick.bind(this);
  }

  static propTypes = {
    userState: PropTypes.object,
    loaderState: PropTypes.object,
    errorState: PropTypes.object,
    getUserData: PropTypes.func
  };

  handleDetailsClick(repositoryName, userName, avatarUrl) {
    this.props.history.push({
      pathname: '/repository',
      state: {
        repositoryName,
        userName,
        avatarUrl
      }
    });
  }

  componentDidMount() {
    const { getUserData, match } = this.props;
    getUserData(match.params.userName);
  }

  render() {
    const { userState, loaderState, errorState } = this.props;
    const { user, repositories } = userState;

    if (errorState) {
      throw new Error(errorState.message);
    }
    return (
      <div className="row">
        {loaderState.isLoading || !user ? (
          <Loader />
        ) : (
          <Fragment>
            <UserDetails user={user} className="my-3 w-100" />
            <RepositoriesSection
              handleDetailsClick={this.handleDetailsClick}
              repositories={repositories}
              className="w-100"
            />
          </Fragment>
        )}
      </div>
    );
  }
}

export { UserPage };
