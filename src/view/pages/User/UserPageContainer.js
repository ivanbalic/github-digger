import { connect } from 'react-redux';
import { UserPage } from './UserPage';
import { getUserData } from '../../../state/actions/userActions';

const mapStateToProps = state => {
  return {
    userState: state.userReducer,
    loaderState: state.loaderReducer,
    errorState: state.errorReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserData: userName => dispatch(getUserData(userName))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPage);
