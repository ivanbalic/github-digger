import React from 'react';
import { PropTypes } from 'prop-types';
import { RepositoryCard } from '../../../components/RepositoryCard/RepositoryCard';

const RepositoriesSection = ({
  repositories,
  handleDetailsClick,
  className
}) => {
  return (
    <div className={className}>
      <h1 className="ml-4">Repositories:</h1>
      <hr className="my-4" />
      <div className="row justify-content-between px-5">
        {repositories.map(repo => (
          <RepositoryCard
            className="border col-5 bg-light my-1 rounded"
            repo={repo}
            handleDetailsClick={handleDetailsClick}
            key={repo.repoId}
          />
        ))}
      </div>
    </div>
  );
};

RepositoriesSection.propTypes = {
  repositories: PropTypes.array,
  handleDetailsClick: PropTypes.func,
  className: PropTypes.string
};

export { RepositoriesSection };
