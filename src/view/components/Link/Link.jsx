import { PropTypes } from 'prop-types';
import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';

import classes from './Link.module.scss';

class Link extends Component {
  static propTypes = {
    path: PropTypes.string,
    onClick: PropTypes.func
  };
  render() {
    const { path, onClick, children } = this.props;
    return (
      <RouterLink className={classes.link} to={path} onClick={onClick}>
        {children}
      </RouterLink>
    );
  }
}

export { Link };
