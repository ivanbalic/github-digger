import React from 'react';
import { PropTypes } from 'prop-types';

import { Button } from '../Button/Button';

const RepositoryCard = ({ repo, handleDetailsClick, className }) => {
  const { repoName, description, owner } = repo;
  const { userName, avatarUrl } = owner;
  return (
    <div className={className}>
      <div className="card-body">
        <h5 className="card-title">{repoName}</h5>
        <p className="card-text">{description}</p>
        <Button
          text="Details"
          onClick={() => handleDetailsClick(repoName, userName, avatarUrl)}
          className="btn btn-secondary"
        />
      </div>
    </div>
  );
};

RepositoryCard.propTypes = {
  repo: PropTypes.object,
  handleDetailsClick: PropTypes.func,
  className: PropTypes.string
};

export { RepositoryCard };
