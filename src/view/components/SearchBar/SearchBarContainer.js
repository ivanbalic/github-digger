import { connect } from 'react-redux';
import { SearchBar } from './SearchBar';
import { onChange } from '../../../state/actions/searchActions';

const mapStateToProps = state => {
  return {
    searchState: state.searchReducer,
    loaderState: state.loaderReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleChange: event => dispatch(onChange(event.target.value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar);
