import { PropTypes } from 'prop-types';
import React, { Component } from 'react';

import classes from './SearchBar.module.scss';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static propTypes = {
    searchState: PropTypes.object,
    loaderState: PropTypes.object,
    handleChange: PropTypes.func
  };

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSearch();
  }

  render() {
    const { searchIcon, searchBar, loading } = classes;
    const { searchState, loaderState, handleChange } = this.props;

    return (
      <form className="col-12" onSubmit={this.handleSubmit}>
        <span className={searchIcon}>
          <i className="fas fa-search" />
        </span>
        <input
          type="text"
          name="search"
          value={searchState.inputValue}
          autoComplete="off"
          placeholder="Search repositories..."
          onChange={event => handleChange(event)}
          className={`${searchBar} ${loaderState.isLoading ? loading : ''}`}
        />
      </form>
    );
  }
}

export { SearchBar };
