import { connect } from 'react-redux';
import { ErrorBoundary } from './ErrorBoundary';
import { updateErrorState } from '../../../state/actions/errorActions';

const mapStateToProps = state => {
  return {
    externalError: state.errorReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onError: error => dispatch(updateErrorState(error))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorBoundary);
