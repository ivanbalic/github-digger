import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends Component {
  state = {
    error: null
  };

  static propTypes = {
    externalError: PropTypes.object,
    onError: PropTypes.func,
    children: PropTypes.node
  };

  static defaultProps = {
    externalError: null,
    onError: f => f
  };

  componentDidCatch(error) {
    this.setState({ error });
    this.props.onError(error);
  }

  render() {
    const { externalError, children } = this.props;
    const { error } = this.state;

    if (externalError || error) {
      return (
        <div className="jumbotron p-4 m-5 border">
          <h1 className="text-center display-4 m-5 ">
            <i className="fab fa-github-alt" />{' '}
            <strong>{externalError.message || error.message}</strong>
            <br />
            We are sorry, something went wrong.
          </h1>
        </div>
      );
    }

    return children;
  }
}

export { ErrorBoundary };
