import React from 'react';
import { PropTypes } from 'prop-types';

const Button = ({ text, className, onClick }) => {
  return (
    <span className={className} onClick={onClick}>
      {text}
    </span>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func
};

export { Button };
