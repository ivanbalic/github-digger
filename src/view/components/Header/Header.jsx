import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <nav className="navbar navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">
        <i className="fab fa-github" /> GitHub Digger
      </Link>
    </nav>
  );
};

export { Header };
