import React from 'react';
import { PropTypes } from 'prop-types';

const Message = ({ text, className }) => {
  return <div className={className}>{text}</div>;
};

Message.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string
};

export { Message };
