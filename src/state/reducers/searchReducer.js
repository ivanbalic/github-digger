const initialState = {
  inputValue: ''
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_INPUT_VALUE':
      state = {
        ...state,
        inputValue: action.payload
      };
      break;

    default:
      break;
  }

  return state;
};

export { searchReducer };
