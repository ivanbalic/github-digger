const initialState = {
  isLoading: false
};

const loaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOADING_START':
      state = {
        isLoading: true
      };
      break;
    case 'LOADING_STOP':
      state = {
        isLoading: false
      };
      break;

    default:
      break;
  }

  return state;
};

export { loaderReducer };
