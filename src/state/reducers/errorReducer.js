const errorReducer = (state = null, action) => {
  switch (action.type) {
    case 'ERROR':
      state = action.payload;
      break;

    default:
      break;
  }
  return state;
};

export { errorReducer };
