const initialState = {
  user: null,
  repositories: []
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_USER':
      state = {
        ...state,
        user: action.payload
      };
      break;
    case 'UPDATE_USER_REPOSITORIES':
      state = {
        ...state,
        repositories: action.payload
      };
      break;

    default:
      break;
  }
  return state;
};

export { userReducer };
