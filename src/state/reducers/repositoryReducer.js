const initialState = {
  repositories: null,
  technologies: [],
  contributors: []
};

const repositoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_REPOSITORIES':
      state = {
        ...state,
        repositories: action.payload
      };
      break;
    case 'UPDATE_TECHNOLOGIES':
      state = {
        ...state,
        technologies: action.payload
      };
      break;
    case 'UPDATE_CONTRIBUTORS':
      state = {
        ...state,
        contributors: action.payload
      };
      break;

    default:
      break;
  }
  return state;
};

export { repositoryReducer };
