const updateErrorState = error => {
  return {
    type: 'ERROR',
    payload: error
  };
};

export { updateErrorState };
