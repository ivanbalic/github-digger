import { startLoading, stopLoading } from './loaderActions';
import { updateErrorState } from './errorActions';
import { userCommunicator } from '../../communicators/UserCommunicator';

const updateUser = user => {
  return {
    type: 'UPDATE_USER',
    payload: user
  };
};

const updateUserRepositories = repositories => {
  return {
    type: 'UPDATE_USER_REPOSITORIES',
    payload: repositories
  };
};

const getUserData = userName => {
  return dispatch => {
    const fetchLIst = [];
    dispatch(startLoading());

    fetchLIst.push(
      userCommunicator.getSingleUser(userName),
      userCommunicator.getUserRepositories(userName)
    );

    Promise.all(fetchLIst)
      .then(([user, repositories]) => {
        dispatch(updateUser(user));
        dispatch(updateUserRepositories(repositories));
        dispatch(stopLoading());
      })
      .catch(error => {
        dispatch(updateErrorState(new Error(error)));
        dispatch(stopLoading());
      });
  };
};

export { getUserData };
