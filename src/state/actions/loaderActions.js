const startLoading = () => {
  return {
    type: 'LOADING_START'
  };
};

const stopLoading = () => {
  return {
    type: 'LOADING_STOP'
  };
};

export { startLoading, stopLoading };
