import { updateErrorState } from './errorActions';
import { startLoading, stopLoading } from './loaderActions';
import { repoCommunicator } from '../../communicators/RepoCommunicator';

const updateRepositories = repositories => {
  return {
    type: 'UPDATE_REPOSITORIES',
    payload: repositories
  };
};
const updateTechnologies = technologies => {
  return {
    type: 'UPDATE_TECHNOLOGIES',
    payload: technologies
  };
};

const updateContributors = contributors => {
  return {
    type: 'UPDATE_CONTRIBUTORS',
    payload: contributors
  };
};

const searchRepositories = searchTerm => {
  return dispatch => {
    dispatch(startLoading());

    repoCommunicator
      .search(searchTerm)
      .then(repositories => {
        dispatch(updateRepositories(repositories));
        dispatch(stopLoading());
        dispatch(updateErrorState(null));
      })
      .catch(error => {
        dispatch(updateErrorState(new Error(error)));
        dispatch(stopLoading());
      });
  };
};

const searchSectionData = (userName, repositoryName) => {
  return dispatch => {
    const fetchList = [];
    dispatch(startLoading());

    fetchList.push(
      repoCommunicator.getTechnologies(userName, repositoryName),
      repoCommunicator.getContributors(userName, repositoryName)
    );
    Promise.all(fetchList).then(([technologies, contributors]) => {
      dispatch(updateTechnologies(technologies));
      dispatch(updateContributors(contributors));
      dispatch(stopLoading());
    });
  };
};

export { searchRepositories, searchSectionData };
