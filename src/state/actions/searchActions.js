const onChange = inputValue => {
  return {
    type: 'UPDATE_INPUT_VALUE',
    payload: inputValue
  };
};

export { onChange };
