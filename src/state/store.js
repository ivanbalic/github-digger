import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware } from 'redux';

import { userReducer } from './reducers/userReducer';
import { loaderReducer } from './reducers/loaderReducer';
import { searchReducer } from './reducers/searchReducer';
import { errorReducer } from './reducers/errorReducer';
import { repositoryReducer } from './reducers/repositoryReducer';

const store = createStore(
  combineReducers({
    userReducer,
    repositoryReducer,
    loaderReducer,
    searchReducer,
    errorReducer
  }),
  {},
  applyMiddleware(thunk)
);

export { store };
