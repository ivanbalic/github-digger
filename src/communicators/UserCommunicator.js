import { User } from '../entities/User';
import { USER_ENDPOINT } from '../shared/endpoints';
import { httpService } from '../services/HttpService';
import { Repository } from '../entities/Repository';

class UserCommunicator {
  getSingleUser(userName) {
    const SINGLE_USER_ENDPOINT = `${USER_ENDPOINT}${userName}`;
    const header = {
      method: 'GET'
    };
    return httpService
      .get(SINGLE_USER_ENDPOINT, header)
      .then(
        ({
          id,
          login,
          avatar_url,
          name,
          email,
          bio,
          followers,
          following,
          location,
          blog
        }) => {
          return new User(
            id,
            login,
            avatar_url,
            name,
            email,
            bio,
            followers,
            following,
            location,
            blog
          );
        }
      );
  }

  getUserRepositories(userName) {
    const USER_REPOS_ENDPOINT = `${USER_ENDPOINT}${userName}/repos`;
    const header = {
      method: 'GET'
    };
    return httpService.get(USER_REPOS_ENDPOINT, header).then(repositories => {
      return repositories.map(repository => {
        const { id, name, description, owner } = repository;
        const { login, avatar_url } = owner;
        const userId = owner.id;
        return new Repository(id, name, description, userId, login, avatar_url);
      });
    });
  }
}

export const userCommunicator = new UserCommunicator();
