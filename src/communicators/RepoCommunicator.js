import { Repository } from '../entities/Repository';
import { Technology } from '../entities/Technology';
import { User } from '../entities/User';
import { httpService } from '../services/HttpService';
import { BASE_ENDPOINT, REPOS_ENDPOINT } from '../shared/endpoints';

class RepoCommunicators {
  search(searchTerm) {
    const SEARCH_REPOS_ENDPOINT = `${BASE_ENDPOINT}search/repositories?q=${searchTerm}`;
    const header = {
      method: 'GET'
    };

    return httpService.get(SEARCH_REPOS_ENDPOINT, header).then(response => {
      const { items } = response;

      return items.map(repository => {
        const { id, name, description, owner } = repository;
        const { login, avatar_url } = owner;
        const userId = owner.id;
        return new Repository(id, name, description, userId, login, avatar_url);
      });
    });
  }

  getTechnologies(userName, repositoryName) {
    const TECHNOLOGIES_ENDPOINT = `${REPOS_ENDPOINT}${userName}/${repositoryName}/languages`;
    const header = {
      method: 'GET'
    };

    return httpService.get(TECHNOLOGIES_ENDPOINT, header).then(technologies => {
      const keys = Object.keys(technologies);
      return keys.map(currentTechName => {
        const currentTechSize = technologies[currentTechName];
        return new Technology(currentTechName, currentTechSize);
      });
    });
  }

  getContributors(userName, repositoryName) {
    const CONTRIBUTORS_ENDPOINT = `${REPOS_ENDPOINT}${userName}/${repositoryName}/contributors`;
    const header = {
      method: 'GET'
    };

    return httpService.get(CONTRIBUTORS_ENDPOINT, header).then(contributors => {
      return contributors.map(
        ({ id, login, avatar_url }) => new User(id, login, avatar_url)
      );
    });
  }
}

export const repoCommunicator = new RepoCommunicators();
